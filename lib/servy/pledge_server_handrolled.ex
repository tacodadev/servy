defmodule Servy.GenericServer do

  def start(callback_module, initial_state, name) do
    pid = spawn(__MODULE__, :listen_loop, [initial_state, callback_module])
    Process.register(pid, name)
    pid
  end

  # Helper Functions

  def call(pid, message) do
     send pid, {:call, self(), message}

     receive do {:response, response} -> response end
   end

   def cast(pid, message) do
     send pid,{:cast, message}
   end

   # Server

   def listen_loop(state, callback_module) do
     # IO.puts "\nWaiting for a message..."

     receive do
       {:call, sender, message} when is_pid(sender) ->
         {response, new_state} = callback_module.handle_call(message, state)
         send sender, {:response, response}
         listen_loop(new_state, callback_module)
       {:cast, message} ->
         new_state = callback_module.handle_cast(message, state)
         listen_loop(new_state, callback_module)
       unexpected ->
         IO.puts "Unexpected message: #{inspect unexpected}"
         listen_loop(state, callback_module)
     end
   end
end

defmodule Servy.PledgeServerHandrolled do

  @name :pledge_server_handrolled

  alias Servy.GenericServer

  # Client Interface Functions

  def start do
    IO.puts "Starting the pledge server..."
    GenericServer.start(__MODULE__, [], @name)
  end

  def create_pledge(name, amount) do
    GenericServer.call @name, {:create_pledge, name, amount}
  end

  def recent_pledges do
    GenericServer.call @name, :recent_pledges
  end

  def total_pledged do
    GenericServer.call @name, :total_pledged
  end

  def clear do
    GenericServer.cast @name, :clear
  end

  # Server Callbacks

  def handle_call(:total_pledged, state) do
    total = Enum.map(state, &elem(&1, 1)) |> Enum.sum
    {total, state}
  end

  def handle_call(:recent_pledges, state) do
    {state, state}
  end

  def handle_call({:create_pledge, name, amount}, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    most_recent_pledges = Enum.take(state, 2)
    new_state = [ {name, amount } | most_recent_pledges ]
    {id, new_state}
  end

  def handle_cast(:clear, _state) do
    []
  end

  defp send_pledge_to_service(_name, _amount) do
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end
end

# alias Servy.PledgeServerHandrolled
#
# pid = PledgeServerHandrolled.start()
#
# # send pid, {:stop, "hammertime"}
#
# IO.inspect PledgeServerHandrolled.create_pledge("larry", 10)
# IO.inspect PledgeServerHandrolled.create_pledge("moe", 20)
# IO.inspect PledgeServerHandrolled.create_pledge("curly", 30)
# IO.inspect PledgeServerHandrolled.create_pledge("daisy", 40)
#
# PledgeServerHandrolled.clear()
#
# IO.inspect PledgeServerHandrolled.create_pledge("grace", 50)
#
# IO.inspect PledgeServerHandrolled.recent_pledges()
#
# IO.inspect PledgeServerHandrolled.total_pledged()

# IO.inspect Process.info(pid, :messages)

# Sometimes you just want a quick and easy way to store state in a process without any hoopla. Anticipating those situations, Elixir provides the Agent module. You can think of it as a simple wrapper around a server process that stores state and offers access to that state through a thin client interface.
# {:ok, agent} = Agent.start(fn -> [] end)
# Agent.update(agent, fn(state) -> [ {"larry", 10} | state ] end)
# Agent.update(agent, fn(state) -> [ {"moe", 20} | state ] end)
# Agent.get(agent, fn(state) -> state end)

# So why didn't we use Agent rather than hand-rolling our own stateful server process? Well, Agent is great if you simply need a process to store state. However, that's all an agent can do! It can't also run asynchronous queries or computations in the server process, such as sending requests to an external service or totaling the pledge amounts.

# More often than not you need a server to store state and do other things related to that state. For that reason, you won't see Agent used very often in the wild. But not to worry: Elixir provides another abstraction called GenServer for building stateful server processes. That's the topic of the next several sections!
