defmodule Servy.Kickstarter do
  use GenServer

  # def start do
  #   IO.puts "Starting the kickstarter..."
  #   GenServer.start(__MODULE__, :ok, name: __MODULE__)
  # end

  def start_link(_arg) do
    IO.puts "Starting the kickstarter..."
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.flag(:trap_exit, true)
    server_pid = start_server()
    {:ok, server_pid}
  end

  def handle_info({:EXIT, _pid, reason}, _state) do
    IO.puts "HTTP server exited (#{inspect reason})"
    server_pid = start_server()
    {:noreply, server_pid}
  end

  defp start_server do
    IO.puts "Starting the HTTP server..."
    # server_pid = spawn(Servy.HttpServer, :start, [4000])
    # Process.link(server_pid)
    port = Application.get_env(:servy, :port)
    server_pid = spawn_link(Servy.HttpServer, :start, [port])
    Process.register(server_pid, :http_server)
    server_pid
  end
end

# In the case of a process terminating normally, the exit signal reason is always the atom :normal. And because the process exited normally, the linked process does not terminate.
# In the case of a process terminating abnormally, the exit signal reason will be anything other than the atom :normal. For example, in the video we used the reason :kaboom.

# Linking Tasks
# pid = Task.async(fn -> Servy.Tracker.get_location("bigfoot") end)
# Task.await(pid)
# pid = Task.async(fn -> raise "Kaboom!" end)
