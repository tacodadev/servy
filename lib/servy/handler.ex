defmodule Servy.Handler do

  @moduledoc "Handles HTTP requests."

  # @pages_path Path.expand("../../pages", __DIR__)
  @pages_path Path.expand("pages", File.cwd!)

  import Servy.Plugins, only: [rewrite_path: 1, log: 1, track: 1]
  import Servy.Parser, only: [parse: 1]
  import Servy.View, only: [render: 3]

  alias Servy.Conv
  alias Servy.BearController
  alias Servy.VideoCam
  # alias Servy.Fetcher

  @doc "Transforms the request into a response."
  def handle(request) do
    request
    |> parse
    |> rewrite_path
    |> log
    |> route
    |> track
    |> format_response
  end

  # def route(conv) do
  #   route(conv, conv.method, conv.path)
  # end
  #
  # def route(conv, "GET", "/wildthings") do
  #   %{ conv | status: 200, resp_body: "Lions, Tigers, Bears" }
  # end
  #
  # def route(conv, "GET", "/bears") do
  #   %{ conv | status: 200, resp_body: "Teddy, Smokey, Paddington" }
  # end
  #
  # def route(conv, "GET", "/bears/" <> id) do
  #   %{ conv | status: 200, resp_body: "Bear #{id}" }
  # end
  #
  # def route(conv, "DELETE", "/bears/" <> _id) do
  #   %{ conv | status: 403, resp_body: "Deleting a bear is forbidden!" }
  # end
  #
  # def route(conv, _method, path) do
  #   %{ conv | status: 404, resp_body: "No #{path} here!" }
  # end

  def route(%Conv{ method: "GET", path: "/sensors" } = conv) do
    # parent = self()
    # caller = self()
    # snapshots = [
    #   Fetcher.async(fn -> VideoCam.get_snapshot("cam-1") end),
    #   Fetcher.async(fn -> VideoCam.get_snapshot("cam-2") end),
    #   Fetcher.async(fn -> VideoCam.get_snapshot("cam-3") end)
    # ] |> Enum.map(&Fetcher.get_result(&1))

    # pid4 = Fetcher.async(fn -> Servy.Tracker.get_location("bigfoot") end)
    # task = Task.async(fn -> Servy.Tracker.get_location("bigfoot") end)

    # snapshots =
    #   ["cam-1", "cam-2", "cam-3"]
    #   |> Enum.map(&Fetcher.async(fn -> VideoCam.get_snapshot(&1) end))
    #   |> Enum.map(&Fetcher.get_result(&1))

    # snapshots =
    #   ["cam-1", "cam-2", "cam-3"]
    #   |> Enum.map(&Task.async(fn -> VideoCam.get_snapshot(&1) end))
    #   |> Enum.map(&Task.await(&1))

    # Task with Timeout
    # task = Task.async(fn -> :timer.sleep(7000); "Done!" end)
    # Task.await(task, 7000)

    # Task with Cutoff Time
    # task = Task.async(fn -> :timer.sleep(8000); "Done!" end)
    # Task.yield(task, 5000)

    # :timer.seconds(5)
    # :timer.minutes(5)
    # :timer.hours(5)

    # Be mindful of blocking calls
    # This can result in unwanted synchronization points
    # snapshot1 = Fetcher.get_result(pid1)
    # snapshot2 = Fetcher.get_result(pid2)
    # snapshot3 = Fetcher.get_result(pid3)

    # where_is_bigfoot = Fetcher.get_result(pid4)
    # where_is_bigfoot = Task.await(task)

    # snapshots = [ snapshot1, snapshot2, snapshot3 ]
    # %{ conv | status: 200, resp_body: inspect {snapshots, where_is_bigfoot} }
    # render(conv, "sensors.eex", snapshots: snapshots, location: where_is_bigfoot)

    sensor_data = Servy.SensorServer.get_sensor_data()

    %{ conv | status: 200, resp_body: inspect sensor_data }
  end

  def route(%Conv{ method: "GET", path: "/kaboom" } = conv) do
    raise "Kaboom!"
  end

  def route(%Conv{ method: "GET", path: "/hibernate/" <> time } = conv) do
    time |> String.to_integer |> :timer.sleep
    %{ conv | status: 200, resp_body: "Awake!" }
  end

  def route(%Conv{ method: "POST", path: "/pledges" } = conv) do
    Servy.PledgeController.create(conv, conv.params)
  end

  def route(%Conv{ method: "GET", path: "/pledges" } = conv) do
    Servy.PledgeController.index(conv)
  end

  def route(%Conv{ method: "GET", path: "/wildthings" } = conv) do
    %{ conv | status: 200, resp_body: "Lions, Tigers, Bears" }
    # The difference is this verifies that the value being updated (conv) is indeed a struct of the type Conv, not a generic map. In this particular case the extra check doesn't gain us anything since the pattern in the function head requires the argument to be a Conv type. But sometimes it's worth adding that extra type safety check.
    # %Conv{ conv | status: 200, resp_body: "Lions, Tigers, Bears" }
  end

  def route(%Conv{ method: "GET", path: "/bears" } = conv) do
    BearController.index(conv)
  end

  def route(%Conv{ method: "GET", path: "/api/bears" } = conv) do
    Servy.Api.BearController.index(conv)
  end

  def route(%Conv{method: "GET", path: "/bears/new"} = conv) do
    @pages_path
      |> Path.join("form.html")
      |> File.read
      |> handle_file(conv)
  end

  def route(%Conv{ method: "GET", path: "/bears/" <> id } = conv) do
    params = Map.put(conv.params, "id", id)
    BearController.show(conv, params)
  end

  def route(%Conv{ method: "POST", path: "/bears" } = conv) do
    BearController.create(conv, conv.params)
  end

  def route(%Conv{ method: "DELETE", path: "/bears/" <> _id } = conv) do
    BearController.delete(conv, conv.params)
  end

  def route(%Conv{method: "GET", path: "/pages/" <> file} = conv) do
    @pages_path
    |> Path.join(file <> ".html")
    |> File.read
    |> handle_file(conv)
  end

  # def route(%{ method: "GET", path: "/about" } = conv) do
  #   file =
  #     Path.expand("../../pages", __DIR__)
  #     |> Path.join("about.html")
  #
  #   case File.read(file) do
  #     {:ok, content} ->
  #       %{ conv | status: 200, resp_body: content }
  #     {:error, :enoent} ->
  #       %{ conv | status: 404, resp_body: "File not found!" }
  #     {:error, reason} ->
  #       %{ conv | status: 500, resp_body: "File error: #{reason}" }
  #   end
  # end

  # Design choice: Case or multi-clause functions?

  def route(%Conv{ method: "GET", path: "/about" } = conv) do
    @pages_path
    |> Path.join("about.html")
    |> File.read
    |> handle_file(conv)
  end

  def route(%Conv{ path: path } = conv) do
    %{ conv | status: 404, resp_body: "No #{path} here!" }
  end

  def route(%Conv{} = conv), do: conv

  def format_response(%Conv{} = conv) do
    # String.length(conv.resp_body)
    """
    HTTP/1.1 #{Conv.full_status(conv)}\r
    Content-Type: #{conv.resp_content_type}\r
    Content-Length: #{byte_size(conv.resp_body)}\r
    \r
    #{conv.resp_body}
    """
  end

  def handle_file({:ok, content}, conv) do
    %{ conv | status: 200, resp_body: content }
  end

  def handle_file({:error, :enoent}, conv) do
    %{ conv | status: 404, resp_body: "File not found!" }
  end

  def handle_file({:error, reason}, conv) do
    %{ conv | status: 500, resp_body: "File error: #{reason}" }
  end

#   Alternatively, you could use a temporary variable for the map, like so:
#
# defp status_reason(code) do
#   codes = %{
#     200 => "OK",
#     201 => "Created",
#     401 => "Unauthorized",
#     403 => "Forbidden",
#     404 => "Not Found",
#     500 => "Internal Server Error"
#   }
#   codes[code]
# end
end
