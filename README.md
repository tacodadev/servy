# Servy

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `servy` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:servy, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/servy](https://hexdocs.pm/servy).


## `Erlang |> Elixir`

Here's a summary of things to keep in mind when transcoding Erlang to Elixir:

Erlang atoms start with a lowercase letter, whereas Elixir atoms start with a colon (:). For example, ok in Erlang becomes :ok in Elixir.

Erlang variables start with an uppercase letter, whereas Elixir variables start with a lowercase letter. For example, Socket in Erlang becomes socket in Elixir.

Erlang modules are always referenced as atoms. For example, gen_tcp in Erlang becomes :gen_tcp in Elixir.

Function calls in Erlang use a colon (:) whereas function calls in Elixir always us a dot (.). For example, gen_tcp:listen in Erlang becomes :gen_tcp.listen in Elixir. (Replace the colon with a dot.)

Last, but by no means least, it's important to note that Erlang strings aren't the same as Elixir strings. In Erlang, a double-quoted string is a list of characters whereas in Elixir a double-quoted string is a sequence of bytes (a binary). Thus, double-quoted Erlang and Elixir strings aren't compatible. So if an Erlang function takes a string argument, you can't pass it an Elixir string. Instead, Elixir has a character list which you can create using single-quotes rather than double-quotes. For example, 'hello' is a list of characters that's compatible with the Erlang string "hello".

```sh
mix run -e "Servy.HttpServer.start(4000)"
```

## Using OTP

Here are a few heuristics to help you get started on the right foot:

Use a Task if you want to perform a one-off computation or query asynchronously.

Use an Agent if you just need a simple process to hold state.

Use a GenServer if you need a long-running server process that store states and performs work concurrently.

Use a dedicated GenServer process if you need to serialize access to a shared resource or service used by multiple concurrent processes.

Use a GenServer process if you need to schedule background work to be performed on a periodic interval.
